import http from 'http'
import Logger from './logger'

class Server {
    private readonly port: number
    private logger: Logger
    private urlMap = {
        '/data': {
            POST: true,
        },
    }
    private codeMap = {
        200: 'OK',
        404: 'Not found',
        500: 'Internal server error',
    }

    constructor(port: number = 8080, logger: Logger) {
        this.port = port
        this.logger = logger
    }

    private getCase() {
        const possibility = Math.random()
        if (possibility < 0.6) {
            return 200
        }
        if (possibility < 0.8) {
            return 500
        }
        return 0
    }

    private processRequest(request, response) {
        const url = request.url || ''
        const method = request.method || ''
        const code =
            this.urlMap[url] && this.urlMap[url][method] ? this.getCase() : 404
        if (code) {
            if (code === 200) {
                let rawData = ''
                request.on('data', (chunk) => {
                    rawData += chunk
                })
                request.on('end', () => {
                    const data = JSON.parse(rawData)
                    this.logger.log(data, true)
                })
            }
            response.writeHead(code, {
                'Content-Type': 'application/json',
                statusMessage: this.codeMap[code],
            })
            response.end()
        }
    }

    run() {
        http.createServer(this.processRequest.bind(this)).listen(this.port)
    }
}

export default Server
