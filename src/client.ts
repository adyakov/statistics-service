import * as https from 'https'
import * as http from 'http'
import Logger from './logger'

class Client {
    private readonly siteUri: string
    private readonly logger: Logger
    private readonly postOptions: object
    private pingId = 1

    constructor(
        siteUri: string,
        serverUrl: string,
        serverPort: number,
        logger: Logger
    ) {
        this.siteUri = siteUri
        this.logger = logger
        this.postOptions = {
            host: serverUrl,
            port: serverPort,
            path: '/data',
            timeout: 10000,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
        }
    }

    getTimeOut(attempt: number): number {
        const delay = 51
        switch (attempt) {
            case 1:
            case 2:
                return delay * Math.floor(Math.random() * attempt * 2)
            default:
                return delay * Math.floor(Math.random() * 9)
        }
    }

    async ping(): Promise<any> {
        try {
            const result = {
                code: 0,
                responseTime: 0,
                date: 0,
                pingId: this.pingId++,
            }
            return await new Promise((resolve, reject) => {
                const startTime = new Date().getTime()
                https.get(this.siteUri, (response) => {
                    response.on('data', () => {})
                    response.on('end', () => {
                        result.code = Number(response.statusCode)
                        result.responseTime = new Date().getTime() - startTime
                        result.date = startTime
                        resolve(result)
                    })
                    response.on('error', (error) => {
                        reject(error)
                    })
                })
            }).then((data) => data)
        } catch (error) {
            this.logger.log(error)
        }
    }

    sendResult({ responseTime, date, pingId, deliveryAttempt = 1 }): void {
        const data: any = {
            pingId: pingId,
            deliveryAttempt: deliveryAttempt,
            date: date,
            responseTime: responseTime,
        }
        this.logger.log(data, true)
        const jsonData = JSON.stringify(data)
        const request = http.request(this.postOptions, (result) => {
            result.on('data', () => {})
            result.on('end', () => {
                data.code = Number(result.statusCode)
                this.logger.log(
                    `PingId:${pingId} DeliveryAttempt:${deliveryAttempt} | Server response code: ${result.statusCode}`
                )
                this.logger.log(data, true, true)
                if (result.statusCode !== 200) {
                    setTimeout(
                        () =>
                            this.sendResult({
                                responseTime,
                                date,
                                pingId,
                                deliveryAttempt: deliveryAttempt + 1,
                            }),
                        this.getTimeOut(deliveryAttempt)
                    )
                }
            })
        })
        request.on('timeout', () => {
            request.destroy()
        })
        request.on('error', (error: any) => {
            if (error.code === 'ECONNRESET') {
                data.code = 408
                this.logger.log(
                    `PingId:${pingId} DeliveryAttempt:${deliveryAttempt} | Server response timeout`
                )
                this.logger.log(data, true, true)
                setTimeout(
                    () =>
                        this.sendResult({
                            responseTime,
                            date,
                            pingId,
                            deliveryAttempt: deliveryAttempt + 1,
                        }),
                    this.getTimeOut(deliveryAttempt)
                )
            }
        })
        request.write(jsonData)
        request.end()
    }
}

export default Client
