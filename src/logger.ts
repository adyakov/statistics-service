import process from 'process'

class Logger {
    private readonly type: string
    private history: object = {}

    constructor(type: string) {
        this.type = type
        process.stdin.resume()
        process.on('SIGINT', () => {
            this.getResult()
            process.exit()
        })
        console.log(`${this.type} started`)
    }

    getAverage(array: any[]): number {
        return (
            array.reduce((prevVal, curr) => prevVal + curr.responseTime, 0) /
            (array.length || 1)
        )
    }

    getResult(): void {
        switch (this.type) {
            case 'server':
                const sortedHistory = Object.values(this.history).sort(
                    (a: any, b: any) =>
                        a.responseTime > b.responseTime ? 1 : -1
                )
                const center = Math.floor(sortedHistory.length / 2)
                const median = center
                    ? sortedHistory.length % 2 !== 0
                        ? sortedHistory[center].responseTime
                        : this.getAverage([
                              sortedHistory[center],
                              sortedHistory[center + 1],
                          ])
                    : 0
                const average = this.getAverage(sortedHistory)
                console.log(``)
                console.log(`median: ${median}`)
                console.log(`average: ${average}`)
                break
            case 'client':
                const result = {
                    200: 0,
                    408: 0,
                    500: 0,
                }
                const clientHistoryArray = Object.values(this.history)
                for (let event of clientHistoryArray) {
                    switch (event.code) {
                        case 200:
                            result['200']++
                            break
                        case 500:
                            result['500']++
                            break
                        default:
                            result['408']++
                            break
                    }
                }
                console.log(``)
                console.log(`total: ${clientHistoryArray.length}`)
                console.log(`200 OK: ${result['200']}`)
                console.log(`500 Error: ${result['500']}`)
                console.log(`408 Timeout: ${result['408']}`)
                break
            default:
                break
        }
        console.log(`${this.type} stopped`)
    }

    log(
        data: any,
        safeHistory: boolean = false,
        silent: boolean = false
    ): void {
        safeHistory
            ? (this.history[`${data.pingId}:${data.deliveryAttempt}`] = data)
            : false
        !silent ? console.log(data) : false
    }
}

export default Logger
