import Client from '../src/client'
import Logger from '../src/logger'

const siteUri = 'https://fundraiseup.com/'
const serverUrl = 'localhost'
const serverPort = 8080
const pingInterval = 1000

const logger = new Logger('client')
const client = new Client(siteUri, serverUrl, serverPort, logger)

const runClient = () =>
    setTimeout(() => {
        client.ping().then((result) => {
            client.sendResult(result)
        })
        runClient()
    }, pingInterval)

runClient()
