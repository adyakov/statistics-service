import Logger from '../src/logger'
import Server from '../src/server'

const port = 8080

const logger = new Logger('server')
const server = new Server(port, logger)

server.run()
